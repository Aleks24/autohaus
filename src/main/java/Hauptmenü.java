import java.util.Scanner;


/**
 * @author Aleksandar Popovic
 */


public class Hauptmenü {

    private Scanner scanner;
    private Autolager autolager;
    private int zwischenErgebnis;



    public Hauptmenü() {

        autolager = new Autolager();
        this.menüAnzeige();


    }



    /**
     * &uuml;berpr&uuml;ft Eingabe und geht dann zur n&auml;chsten Klasse
     */


    public void menüAnzeige() {

        System.out.println("Herzlich Wilkommen bei der Autohaus GmbH");
        System.out.println(" ");
        System.out.println("Um Ihre Konfiguration zu starten geben Sie bitte die 1 ein");
        scanner = new Scanner(System.in);

        int i = scanner.nextInt();

        if (i==1) {

            this.autoAuswahl();

        }

        else {

            System.out.println("Vielen Dank für Ihren Besuch. Bis zum nächsten mal");


        }

    }



    /**
     * Gibt alle Autos aus was in der Klasse Autolager vorhanden sind
     * &uuml;berpr&uuml;ft ob Eingabe gr&ouml;&szlig;er 0 und kleiner 4 ist und geht dann zur n&auml;chsten Methode
     */


    public void autoAuswahl(){

        System.out.println("Bitte geben Sie nun den Namen Ihres Autos ein: ");

        autolager.austattungsLager();
        autolager.ausgabe();

        int i = scanner.nextInt();

        if (i < 4 && i > 0) {

            int zwischenPreisAuto = i;
            austattungAuswahl();


        } else {

            System.out.println("Vielen Dank für Ihren Besuch. Bis zum nächsten mal");
            this.menüAnzeige();

        }




    }


    /**
     * Gibt alle Austattungen aus was in der Klasse Autolager vorhanden sind
     * &uuml;berpr&uuml;ft ob Eingabe gr&ouml;&szlig;er 0 und kleiner 4 ist und geht dann zur n&auml;chsten Methode
     */


    public void austattungAuswahl() {

        System.out.println("Bitte geben Sie nun Ihr gewünschtes Austattung ein");


        autolager.austattungAusgabe();



        int i = scanner.nextInt();

        if (i < 4 && i > 0) {

            this.zwischenErgebnis = i;
            this.bestätigen();


        } else {

            System.out.println("Vielen Dank für Ihren Besuch. Bis zum nächsten mal");
            this.menüAnzeige();

        }


    }


    /**
     * Gibt aus welches Auto man ausgew&auml;hlt hat
     * Gibt aus welche Austattung man ausgew&auml;hlt hat
     * Gibt den Endpreis aus
     * &Uuml;berpr&uuml;ft ob Eingabe mit J &uuml;bereinstimmt
     */


    public void bestätigen() {

        System.out.println("Sie haben folgendes Auto ausgewählt: " + this.autolager.autoAufrufen(zwischenErgebnis));


        System.out.println("Sie haben folgende Austattung ausgewählt: " + this.autolager.ausstattungAufrufen(zwischenErgebnis));


        System.out.println("Ihr persönlich konfiguriertes Auto kostet: " + this.autolager.gesamtPreis(zwischenErgebnis) + "€");

        System.out.println("");

        System.out.println("Wollen Sie dieses Auto kaufen? J/N");


        String ja = scanner.next();



        if (ja.contains("J") || ja.contains("j")){

            System.out.println("Vielen Dank für Ihre Bestellung");


        } else {

            System.out.println("Vielen Dank für Ihren Besuch. Bis zum nächsten mal!");

            this.menüAnzeige();

        }



    }


}

