import java.util.ArrayList;
import java.util.Scanner;

public class Autolager {

    private ArrayList<Car> list = new ArrayList<>();
    private ArrayList<Austattung> list1 = new ArrayList<>();
    private Car bmw;
    private Car mercedes;
    private Car audi;
    private Austattung comfort;
    private Austattung sport;
    private Austattung luxus;



    /**
     * Füllt list mit Autos
     */

    public Autolager() {


        Car bmw = new Car("Bmw", 52000);
        Car mercedes = new Car("Mercedes", 48000);
        Car audi = new Car("Audi", 45000);

        list.add(bmw);
        list.add(mercedes);
        list.add(audi);


    }


    /**
     * Prüft ob etwas in list vorhanden ist und gibt sie dann aus
     */


    public void ausgabe()  {

        for(int i = 0; i < list.size(); i++) {

            System.out.println((i+1) + " " + list.get(i).getName() + " " + list.get(i).getPreis() + " €");


        }


    }


    /**
     * Füllt list1 mit Austattung
     */


    public void austattungsLager(){

        Austattung comfort = new Austattung("Comfort", 4000);
        Austattung sport = new Austattung("Sport", 8000);
        Austattung luxus = new Austattung("Luxus", 16000);

        list1.add(comfort);
        list1.add(sport);
        list1.add(luxus);


    }



    /**
     * Prüft ob etwas in list1 vorhanden ist und gibt Sie dann aus
     */


    public void austattungAusgabe()  {

        for(int i = 0; i < list1.size(); i++) {

            System.out.println((i+1) + " " + list1.get(i).getName() + " " + list1.get(i).getPreis() + " €");


        }

    }


    /**
     * Gibt ausgew&auml;hltes Auto aus
     * @param i ist die Nummer des Autos
     * @return gibt das Auto zur&uuml;ck
     */


    public String autoAufrufen(int i) {


        return list.get(i-1).getName();

    }


    /**
     * Gibt ausgew&auml;hlte Austattung
     * @param i
     * @return
     */


    public String ausstattungAufrufen(int i) {

        return list1.get(i-1).getName();
    }



    /**
     * Rechnet den Preis den Autos und der Austattung zusammen und gibt es dann aus
     * @param i ist die Nummer des ausgew&auml;hlten Autos und die Nummer der ausgew&auml;hlten Austattung
     * @return gibt gesamtpreis aus
     */



    public String gesamtPreis(int i) {

        int gesamtPreis = list.get(i-1).getPreis() + list1.get(i-1).getPreis();

        return Integer.toString(gesamtPreis);
    }






}
