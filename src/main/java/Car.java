public class Car {

    private String name;
    private int preis;
    private String austattung;


    public Car(String name, int preis) {

        this.name = name;
        this.preis = preis;

    }


    public String getName() {

        return name;
    }



    public void setName(String name) {

        this.name = name;

    }


    public int getPreis() {

        return preis;

    }


    public void setPreis(int preis) {

        this.preis = preis;

    }


    public String getAustattung() {

        return austattung;
    }


    public void setAustattung(String austattung) {

        this.austattung = austattung;

    }
}
